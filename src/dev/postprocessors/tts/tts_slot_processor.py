from src.dev.postprocessors.tts.tts_slot_utils import TTSSlotUtils

import logging

logger = logging.getLogger(__name__)


class PostProcessTTSSlot(object):
    """
    a key of process_functions must be a slot in template_actions.json,
    which need to post-process after publish a TTS-slot

    For sync, this function also need to re-use to update slot with record-type,
    which declared in method "update" of slot-record.
    """
    process_functions = {
        "get_phone_split": TTSSlotUtils.process_customer_phone
    }

    @staticmethod
    def get_process_function_by_name(name):
        return PostProcessTTSSlot.process_functions.get(name)

    @staticmethod
    def process_tts(slot_name, raw_value):
        if raw_value is None:
            return "none"
        new_value = raw_value
        if slot_name in PostProcessTTSSlot.process_functions:
            process_function = PostProcessTTSSlot.get_process_function_by_name(slot_name)
            new_value = process_function(new_value)
        else:
            logger.warning(f"TTS slot `{slot_name}` must be define in post-process functions"
                           f" for TTS initialization in generating audio before the conversation")
        return new_value
