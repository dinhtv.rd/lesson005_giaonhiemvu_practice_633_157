class ImplExternalModule:
    def __init__(self):
        self.name = self.name()

    def name(self):
        raise NotImplementedError("An ExternalModule must implement its `name` which is the name of metadata")

    def process(self, message, recorder=None):
        raise NotImplementedError("An ExternalModule must implement its process method")
