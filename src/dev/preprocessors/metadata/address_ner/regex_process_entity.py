import os
import csv
import re
import copy
import logging
from typing import List, Dict, Tuple

# from src.dev.utils.io import jsonio
import json

logger = logging.getLogger(__name__)


class RegexProcessEntity:
    """docstring for RegexFeature"""

    def __init__(self, path_regex, dir_address):
        with open(path_regex, "r", encoding='utf8') as file:
            self.regex_feature = json.load(file)
        self.load_data(dir_address)
        self.pattern = {}

    def load_data(self, dir_address) -> None:
        if self.regex_feature is None:
            return
        for name_action in self.regex_feature:
            for entity in self.regex_feature.get(name_action):
                # load pattern of regex into regex_feature
                if self.regex_feature[name_action][entity].get('pattern') is None:
                    self.regex_feature[name_action][entity]['pattern'] = []
                for lookup in self.regex_feature.get(name_action).get(entity).get('lookup'):
                    with open(os.path.join(dir_address, lookup), 'r') as file:
                        for data in file:
                            if data.find("<!--") != -1 or data.find("##") != -1:
                                continue
                            data = data.replace("\n", "")
                            if len(data) == 0:
                                continue
                            data = data.replace("- ", "")
                            self.regex_feature[name_action][entity]['pattern'].append(data)

    def get_extract_entities(self, message: str, regex_feature: dict, status_space: bool = False) -> List[dict]:
        """
        Extract entity
        :param message:
        :param regex_feature:
        :param status_space: Status add syntax "\\b {...} \\b"
        :return: Array entity
        """
        entities = {}
        # thực hiện match các pattern của các entity với đầu vào message
        for name_entity in regex_feature:
            list_pattern = regex_feature.get(name_entity).get('pattern')
            entities = self.match_regex(message, list_pattern, name_entity, entities, status_space)
        list_extracted_all = []
        # flatten entities lại thành 1 list
        for name in entities:
            list_e = entities[name].copy()
            list_extracted_all.extend(list_e)
        return list_extracted_all

    def match_regex(self, message: str, list_pattern: List[str], name_entity: str, entities: dict,
                    status_space: bool) -> Dict:
        """
        Regex name enity follow list pattern
        :param message: Text
        :param list_pattern: Array pattern which using regex
        :param name_entity: name of entity regex
        :param entities: Result entity after regex
        :param status_space:
        :return entities (Dict): list entity regex follow name_entity
        """
        for d in list_pattern:
            if status_space == True:
                d_tmp = "\\b" + d + "\\b"
            else:
                d_tmp = d
            pattern = re.compile(d_tmp)
            for match in pattern.finditer(message.lower()):
                name = name_entity
                if name == "district":
                    if len(match.group()) == 0:
                        logger.debug("========Syntax=======: {}".format(d))
                entity = {
                    "start": match.start(),
                    "end": match.end(),
                    "value": match.group(),
                    "confidence": 1.0,
                    "entity": name,
                }
                if name not in entities:
                    entities[name] = []
                entities[name].append(entity)
        return entities

    def remove_entites_overlap(self, extracted_all: List[dict]) -> List[dict]:
        """
        Remove overlap entity
        :param extracted_all: Array entity which regex
        :return:
        extracted_final: Array entity after remove overlap
        """
        extracted = copy.deepcopy(extracted_all)
        extracted_final = []
        index_remove = []
        for i in range(len(extracted)):
            extract_1 = extracted[i]
            if i in index_remove:
                continue
            for j in range(len(extracted)):
                if j in index_remove or j == i:
                    continue
                extract_2 = extracted[j]
                # extract_2 nằm trong extract_1
                if extract_1["start"] <= extract_2["start"] and extract_1["end"] >= extract_2["end"]:
                    index_remove.append(j)

        for i in range(len(extracted)):
            if i not in index_remove:
                extracted_final.append(extracted[i])
        return extracted_final

    def get_entities_good(self, list_extracted, name_entities, same_size=None) -> Dict:
        """
        :param list_extracted:
        :param name_entities:
        :param same_size:
        :return:
        """
        value = ""
        entities = None
        count = 0
        for extract in list_extracted:
            if extract.get('entity') == name_entities:
                if len(value) == len(extract.get('value')):
                    count += 1
                else:
                    if len(value) < len(extract.get('value')):
                        value = extract.get('value')
                        entities = extract
                    if same_size == True:
                        count = 1
                    else:
                        count += 1
        if len(value) == 0:
            return None, None
        return entities, count < 2

    def remove_stop_word_from_mesage(self, message, list_extracted) -> str:
        """
        Replace stop word in message into space with len(space) == len(value)
        :param message:
        :param list_extracted:
        :return:
        """
        message_tmp = copy.deepcopy(message)
        for extract in list_extracted:
            if extract.get('entity') == 'stop_word':
                text_replace = ""
                for i in range(extract.get('end') - extract.get('start')):
                    text_replace += " "
                message_tmp = message_tmp[:extract.get('start')] + text_replace + message_tmp[extract.get('end'):]
        return message_tmp

    def remove_text_from_entities(self, message, entities: List[dict]) -> str:
        """
        Replace value entity into space with len(space) == len(value)
        :param message:
        :param entities:
        :return:
        """
        message_tmp = copy.deepcopy(message)
        for extract in entities:
            text_replace = ""
            for i in range(extract.get('end') - extract.get('start')):
                text_replace += " "
            message_tmp = message_tmp[:extract.get('start')] + text_replace + message_tmp[extract.get('end'):]
        return message_tmp

    def process(self, message, name_object, same_size=None, status_space=False):
        """
        param:
            message: text message send
            name_object: đối tượng muốn regex entities
        return dict gồm key-value tương ứng name_entity-value
        """
        if self.regex_feature is None:
            return None, None
        list_extracted_all = self.get_extract_entities(message, self.regex_feature.get(name_object), status_space)
        list_extracted = self.remove_entites_overlap(list_extracted_all)
        if list_extracted is not None:
            list_extracted = sorted(list_extracted, key=lambda i: i['start'])
        logger.debug("============List Extracted======: {}".format(list_extracted))
        entities = {}
        for name_entity in self.regex_feature.get(name_object):
            if name_entity == "stop_word" or entities.get("name_entity") is not None:
                continue
            entity, status_mutil_value = self.get_entities_good(list_extracted, name_entity, same_size)
            if entity is not None:
                entities[name_entity] = entity.get("value")
        return entities, status_mutil_value

    def process_entity(self, message, name_object, same_size=None, status_space=False):
        """
        param:
            message: text message send
            name_object: đối tượng muốn regex entities
        return dict gồm key-value tương ứng name_entity-value
        """
        if self.regex_feature is None:
            return None, None
        list_extracted_all = self.get_extract_entities(message, self.regex_feature.get(name_object), status_space)
        list_extracted = self.remove_entites_overlap(list_extracted_all)
        logger.debug("============List Extracted======: {}".format(list_extracted))
        entities = {}
        for name_entity in self.regex_feature.get(name_object):
            if name_entity == "stop_word" or entities.get("name_entity") is not None:
                continue
            entity, status_mutil_value = self.get_entities_good(list_extracted, name_entity, same_size)
            if entity is not None:
                entities[name_entity] = entity
        return entities, status_mutil_value

# #
# if __name__ == '__main__':
#     regex_process_entity = RegexProcessEntity("resources/nlu/ner/regex_address/regex_feature_address.json", "resources/nlu/ner/regex_address")
#     message = "cam hiếu cam lộ"
#     output = regex_process_entity.process(message, "ObjectAddress")
#     print(output)
