import os
import re
import json
import copy
import logging

logger = logging.getLogger(__name__)


class NormalizeNumber():
    """docstring for NormalizeNumber"""

    def __init__(self, regex_feature, text2number):
        self.regex_feature = regex_feature
        self.text2number = text2number
        self.key_units = ["trăm", "nghìn", "triệu", "tỷ", "nghìn rưỡi", "triệu rưỡi", "tỷ rưỡi", "rưỡi"]

    def process_units_hundred(self, group):
        """
        Method: đưa từ value of entities extract về dưới dạng số
        """
        output = []
        for entity in group:
            if entity.get("value") in self.key_units:
                output.append(entity)
                continue
            entity = self.process_entity_into_number(entity)
            if isinstance(entity.get("value"), int) or isinstance(entity.get("value"), float):
                output.append(entity)
            else:
                if entity.get("value") in self.key_units:
                    output.append(entity)
        return output

    def remove_entites_overlap_part(self, extracted_all):
        extracted = copy.deepcopy(extracted_all)
        extracted_final = []
        for entity in extracted:
            if len(extracted_final) == 0:
                extracted_final.append(entity)
                continue
            start = extracted_final[-1].get("start")
            end = extracted_final[-1].get("end")
            if entity["start"] <= start and entity["end"] >= end:
                extracted_final[-1] = entity
                continue
            if (start <= entity.get("start") and end >= entity.get("start")) or (
                    start <= entity.get("end") and end >= entity.get("end")):
                continue
            extracted_final.append(entity)
        return extracted_final

    def process(self, content):
        list_extracted_all = self.regex_feature.get_extract_entities(content,
                                                                     self.regex_feature.regex_feature.get("NormNumber"))
        list_extracted_all = sorted(list_extracted_all, key=lambda i: i['start'])
        list_extracted = self.remove_entites_overlap_part(list_extracted_all)
        group_entities = self.cluster_number(list_extracted)
        if group_entities is None or len(group_entities) == 0:
            return content
        output = []
        for group in group_entities:
            group = self.process_units_hundred(group)
            elements = self.process_operate(group)
            if elements is None:
                logger.debug("===Error in method process_operate with input===: {}".format(content))
            if elements is not None and len(elements) > 0:
                output.extend(elements)
        # return self.fill_text_replace(content, output) ## not use replace symbol_float 
        ## ##replace chấm, phảy phẩy
        output_text = self.fill_text_replace(content, output)
        return self.process_symbo_float(output_text)  ##replace chấm, phảy phẩy

    def cluster_number(self, entities):
        """
        Method: Phân cụm các number entities liên tiếp lại thành nhóm
        """
        if entities is None or len(entities) == 0:
            return None
        group_entities = []
        group = []
        for entity in entities:
            if len(group) == 0:
                group.append(entity)
                continue
            if entity.get("start") == group[-1].get("end") + 1:
                group.append(entity)
            else:
                group_entities.append(group)
                group = [entity]
        if len(group) != 0:
            group_entities.append(group)
        return group_entities

    def process_operate(self, groups):
        if len(groups) == 1:
            if groups[0].get("value") in self.key_units and groups[0].get("value") not in ["trăm", "nghìn", "rưỡi",
                                                                                           "tỷ", "triệu"]:
                if self.text2number.get(groups[0].get("value")) is not None:
                    groups[0]["value"] = self.text2number.get(groups[0].get("value"))
            return groups
        group = copy.deepcopy(groups)
        output = []
        extract = None
        mutil = 1
        group = sorted(group, key=lambda i: i['start'], reverse=True)
        try:
            for entity in group:
                if entity.get("value") in self.key_units:
                    units = float(self.text2number.get((entity.get("value"))))
                    if units <= mutil:
                        mutil = mutil * units
                    else:
                        mutil = units
                    if extract is None:
                        extract = entity
                        extract["value"] = None
                    else:
                        extract["start"] = entity.get("start")
                else:
                    if extract is None:
                        extract = entity
                    else:
                        if extract.get("value") is None:
                            if mutil % 15 == 0 or mutil == 1.5:  ## thêm phần xử lý rưỡi
                                mutil = mutil / 15 * 10
                                extract["value"] = entity.get("value") * mutil + mutil * 0.5
                            else:
                                extract["value"] = entity.get("value") * mutil
                        else:
                            if mutil > extract.get("value"):
                                if mutil % 15 == 0 or mutil == 1.5:  ## thêm phần xử lý rưỡi
                                    mutil = mutil / 15 * 10
                                    extract["value"] += entity.get("value") * mutil + mutil * 0.5
                                else:
                                    extract["value"] += entity.get("value") * mutil
                            else:
                                output.append(extract)
                                extract = entity
                        extract["start"] = entity.get("start")
            if extract is not None and extract.get("value") is not None:
                output.append(extract)
            output = sorted(output, key=lambda i: i['start'])
            return output
        except Exception as e:
            return None

    def process_entity_into_number(self, entity):
        """

        """
        entity["value"] = entity.get("value").replace(",", ".")
        value = entity.get("value")
        if value in self.key_units:
            return entity
        if self.check_number(value):
            number = float(value)
        else:
            number = self.text2number.get(value)
        if number is None:
            logger.debug("========Value not exit=======: {}".format(entity.get("value")))
            return entity
        entity["value"] = float(number)
        return entity

    def fill_text_replace(self, text, extrators):
        output = ""
        start = 0
        result = []
        for extract in extrators:
            result.append(text[start:extract.get("start")])
            try:
                if float(extract.get("value")) == int(extract.get("value")):
                    value = str(int(extract.get("value")))
                else:
                    value = str(extract.get("value"))
            except Exception as e:
                value = str(extract.get("value"))
            result.append(value)
            start = extract.get("end")
        result.append(text[start:len(text)])
        output = " ".join(result)
        return self.replace_space(output)

    def replace_space(self, text):
        """
        Relace khoảng các khoảng trống đi
        """
        tokens = text.split(" ")
        result = []
        for token in tokens:
            if len(token.strip()) == 0:
                continue
            result.append(token.strip())
        output = " ".join(result)
        return output

    def check_number(self, text):
        if text is None:
            return False
        text = text.replace(",", ".")
        try:
            value = float(text)
            return True
        except Exception as e:
            return False

    def process_symbo_float(self, text):
        if text is None:
            return None
        list_pattern = ["([0-9]+ (phảy|phẩy|chấm) [0-9]+)"]
        entities = self.regex_feature.match_regex(text, list_pattern, "symbol_float", {}, True)
        if entities is None or entities.get("symbol_float") is None or len(entities.get("symbol_float")) == 0:
            return text
        result = []
        for entity in entities.get("symbol_float"):
            if entity.get("value").find(".") != -1 or entity.get("value").find(",") != -1:
                continue
            token_value = entity.get("value").split(" ")
            entity["value"] = token_value[0] + "." + token_value[-1]
            result.append(entity)
        if len(result) == 0:
            return text
        return self.fill_text_replace(text, result)
