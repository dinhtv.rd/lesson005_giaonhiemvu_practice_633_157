from datetime import datetime, timedelta

from src.dev.preprocessors.metadata.datetime_extractor.define import (
    DATETIME_FORMAT,
    DATE_FORMAT,
    TIME_FORMAT,
    BACKWARD,
)
from src.dev.preprocessors.metadata.datetime_extractor.duckling.duckling_wrapper import DucklingWrapper
from src.dev.preprocessors.metadata.datetime_extractor.selector import Selector
from config import ENDPOINT_DUCKLING


class DucklingDatetimeExtractor(object):
    def __init__(self,
                 datetime_format=None,
                 date_format=None,
                 time_format=None) -> None:
        self.datetime_format = datetime_format if datetime_format else DATETIME_FORMAT
        self.date_format = date_format if date_format else DATE_FORMAT
        self.time_format = time_format if time_format else TIME_FORMAT
        self.extractor_name = "Duckling"
        self.wrapper = DucklingWrapper(ENDPOINT_DUCKLING)
        self.selector = Selector()

    async def process(self,
                      message,
                      date_from=None,
                      date_to=None,
                      time_from=None,
                      time_to=None,
                      priority_direction=None):
        result = {
            "date": None,
            "time": None,
            "range": None,
            "values": [],
            "extractor": self.extractor_name,
            "status_date": 400,
            "status_time": 400
        }

        extracted = await self.wrapper.extract(message)
        for entity in extracted:
            value = entity.get("value")
            if isinstance(value, dict) and "from" in value and "to" in value:
                result["range"] = value
                result["values"].append(
                    {
                        "type": "range",
                        "value": value
                    }
                )
                continue
            date_extracted = self.extract_date(value)
            time_extracted = self.extract_time(value)
            result["values"].append(
                {
                    "type": "date",
                    "value": date_extracted
                }
            )
            result["values"].append(
                {
                    "type": "time",
                    "value": time_extracted
                }
            )
            date = self.selector.better_date(
                date_old=result.get("date"),
                date_new=date_extracted,
                date_from=date_from,
                date_to=date_to,
                priority_direction=priority_direction,
                status_old=result["status_date"]
            )
            time = self.selector.better_time(
                time_old=result.get("time"),
                time_new=time_extracted,
                time_from=time_from,
                time_to=time_to,
                priority_direction=priority_direction,
                status_old=result["status_time"]
            )
            result["date"] = date.get("value")
            result["time"] = time.get("value")
            if result["status_date"] == 400 or \
                    result["status_date"] == 401 and date.get("status") == 200:
                result["status_date"] = date.get("status")

            if result["status_time"] == 400 or \
                    result["status_time"] == 401 and time.get("status") == 200:
                result["status_time"] = time.get("status")
        return result

    def extract_date(self, datetime_value):
        if datetime_value is None:
            return None
        datetime_extracted = datetime.strptime(datetime_value, self.datetime_format)
        return datetime_extracted.strftime(self.date_format)

    def extract_time(self, datetime_value):
        if datetime_value is None:
            return None
        datetime_extracted = datetime.strptime(datetime_value, self.datetime_format)
        return datetime_extracted.strftime(self.time_format)
