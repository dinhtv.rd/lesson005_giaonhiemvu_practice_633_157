import os
from config import DIR_NER_RESOURCE

PATH_RULE_DATETIME_REGEX = os.path.join(DIR_NER_RESOURCE, "datetime_extractor/rule_datetime_regex.json")
PATH_RULE_DATETIME_SIGNAL = os.path.join(DIR_NER_RESOURCE, "datetime_extractor/rule_datetime_signal.json")
MAX_DAYS = 3650000

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f%z"
DATE_FORMAT = "%d/%m/%Y"
TIME_FORMAT = "%H:%M:%S"
FORWARD = "forward"
BACKWARD = "backward"
PIPELINE = [
    "Rule"
]
