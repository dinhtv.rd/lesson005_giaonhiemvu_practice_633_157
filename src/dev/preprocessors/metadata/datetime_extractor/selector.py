from datetime import datetime, timedelta

from src.dev.preprocessors.metadata.datetime_extractor.define import (
    DATETIME_FORMAT,
    DATE_FORMAT,
    TIME_FORMAT,
    BACKWARD
)


class Selector(object):
    def __init__(self,
                 datetime_format=None,
                 date_format=None,
                 time_format=None) -> None:
        self.datetime_format = datetime_format if datetime_format else DATETIME_FORMAT
        self.date_format = date_format if date_format else DATE_FORMAT
        self.time_format = time_format if time_format else TIME_FORMAT

    def better_time(self, time_old, time_new, time_from, time_to, priority_direction, status_old):
        lts = [time_old, time_new, time_from, time_to]
        dt_lts = []
        for t in lts:
            try:
                dt = datetime.strptime(t, self.time_format)
                dt_lts.append(dt)
            except:
                dt_lts.append(None)
        dt_time_old, dt_time_new, dt_time_from, dt_time_to = tuple(dt_lts)
        if dt_time_from and dt_time_to:
            is_inside = dt_time_new and dt_time_from <= dt_time_new <= dt_time_to
            if not is_inside and dt_time_new is not None:
                if dt_time_new <= datetime.strptime("12:00:00", self.time_format):
                    dt_time_new += timedelta(hours=12)
                    time_new = dt_time_new.strftime(self.time_format)
                else:
                    return {
                        "value": time_old,
                        "status": 401
                    }
                is_inside = dt_time_new and dt_time_from <= dt_time_new <= dt_time_to
                if not is_inside:
                    return {
                        "value": time_old,
                        "status": 401
                    }
        if priority_direction != BACKWARD:
            if dt_time_old is None or dt_time_new > dt_time_old:
                return {
                    "value": time_new,
                    "status": 200
                }
        else:
            if dt_time_old is None or dt_time_new < dt_time_old:
                return {
                    "value": time_new,
                    "status": 200
                }
        return {
            "value": time_old,
            "status": status_old
        }

    def better_date(self, date_old, date_new, date_from, date_to, priority_direction, status_old):
        lds = [date_old, date_new, date_from, date_to]
        dt_lds = []
        for d in lds:
            try:
                dt = datetime.strptime(d, self.date_format)
                dt_lds.append(dt)
            except:
                dt_lds.append(None)

        dt_date_old, dt_date_new, dt_date_from, dt_date_to = tuple(dt_lds)
        if dt_date_from and dt_date_to:
            is_inside = dt_date_new and dt_date_from <= dt_date_new <= dt_date_to
            if not is_inside:
                return {
                    "value": date_old,
                    "status": 401
                }
        if priority_direction != BACKWARD:
            if dt_date_old is None or dt_date_new > dt_date_old:
                return {
                    "value": date_new,
                    "status": 200
                }
        else:
            if dt_date_old is None or dt_date_new < dt_date_old:
                return {
                    "value": date_new,
                    "status": 200
                }
        return {
            "value": date_old,
            "status": status_old
        }
