from src.core.condition.interfaces.impl_record import (
    ImplCheckFlow,
    ImplUpdateLocal,
    ImplUpdateGlobal,
    ImplUpdateSlot
)
from src.dev.context.custom_condition_classes import *
