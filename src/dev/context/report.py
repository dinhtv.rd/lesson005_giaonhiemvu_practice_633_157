import logging
import copy

from src.core.condition.interfaces.impl_record import ImplUpdateGlobal
from src.core.condition.condition_fixed import check_error

logger = logging.getLogger(__name__)
ROOT_REPORT_INPUT_SLOTS = "input"


class GetReport(ImplUpdateGlobal):
    def name(self):
        return "get_report"

    def update_check_correct_answer(self, recorder):
        id_correct = "correct_answer"
        id_incorrect = "incorrect_answer"
        correct_answer = recorder.get_current_record().get(id_correct)
        incorrect_answer = recorder.get_current_record().get(id_incorrect)

        if correct_answer:
            self.record["check_correct_answer"] = True
        elif incorrect_answer:
            self.record["check_correct_answer"] = False
        else:
            self.record["check_correct_answer"] = None

        if recorder.get_current_record().get(id_correct) is not None:
            recorder.custom_records[id_correct].record = None

        if recorder.get_current_record().get(id_incorrect) is not None:
            recorder.custom_records[id_incorrect].record = None

    def update_nlp_error(self, recorder):
        fluency_error = recorder.get_current_record().get("extract_fluency_error")
        wording_error = recorder.get_current_record().get("extract_wording_error")
        grammar_error = check_error.process_check_grammar(message=recorder.get_last_message())
        self.set_param_nlp_error(fluency_error)
        self.set_param_nlp_error(wording_error)
        self.set_param_nlp_error(grammar_error)

    def process(self, recorder, args):
        self.update_check_correct_answer(recorder)
        return self.record

    def set_param_nlp_error(self, data_error: list) -> None:
        if data_error is None or len(data_error) == 0:
            return
        for element in data_error:
            type_error = element.get("type_error")
            if type_error is None:
                logger.debug(f"WARNING: TYPE ERROR IS NONE {data_error}")
                return
            if self.record.get("nlp_error_grammar").get(type_error) is None:
                self.record["nlp_error_grammar"][type_error] = []
            if element not in self.record["nlp_error_grammar"][type_error]:
                self.record["nlp_error_grammar"][type_error].append(copy.deepcopy(element))

    def __init__(self):
        super().__init__()
        self.record = {
            "check_correct_answer": None,
            "nlp_error_grammar": {}
        }


class SubscriberForward(ImplUpdateGlobal):
    def name(self):
        return "get_subscriber"

    def process(self, recorder, args):
        return None

    def __init__(self):
        super().__init__()
        self.record = {
        }
