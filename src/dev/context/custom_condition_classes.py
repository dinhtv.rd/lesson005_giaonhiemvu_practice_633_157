import json, re
import logging

from src.core.condition.interfaces.impl_record import (
    ImplCheckFlow,
    ImplUpdateLocal,
    ImplUpdateGlobal,
    ImplUpdateSlot
)


logger = logging.getLogger(__name__)


def regex_by_pattern(pattern, message):
    logger.debug(f"[REGEX_PATTERN] Try to regex message '{message}' with pattern '{pattern}'")
    if pattern.get("black_regex") is not None:
        for pattern in pattern.get("black_regex"):
            if re.search(pattern=pattern, string=message, flags=re.IGNORECASE):
                logger.debug(f"[REGEX_PATTERN] Match BLACK pattern in '{message}' with pattern '{pattern}': "
                             f"FAIL to match!")
                return False
    for pattern in pattern.get("regex"):
        if re.search(pattern=pattern, string=message, flags=re.IGNORECASE):
            logger.debug(f"[REGEX_PATTERN] Match WHITE pattern in '{message}' with pattern '{pattern}': "
                         f"SUCCESS to match!")
            return True
    logger.debug(f"[REGEX_PATTERN] Don't match any pattern in '{message}' with pattern '{pattern}': ")
    return False
