import logging

logger = logging.getLogger(__name__)

class ImplRecord(object):
    def __init__(self):
        self.record = None

    def name(self):
        raise NotImplementedError("A flow must implement its name")

    def type(self):
        raise NotImplementedError("A flow must implement its type")

    def process(self, recorder, args):
        """
        check_update and update from values of recorder
        :param recorder: Recorder
        :param args: args
        :return: None
        """
        raise NotImplementedError("A flow must implement its process method")

    def update(self, recorder, args=None):
        self.record = self.process(recorder, args)
        logger.debug(f"[CONDITION] Update {self.name()} = {self.record}")

    def reset(self) -> None:
        """
        reset value of item record when recorder run reset
        :return: None
        """
        self.record = None

    def set_record(self, record):
        self.record = record

    def get_value(self):
        return self.record


class ImplCheckFlow(ImplRecord):
    def type(self):
        return "check"

    def name(self):
        raise NotImplementedError("A check flow must implement its name")

    def process(self, recorder, args) -> bool:
        """
        check_update and update from values of recorder
        :param recorder: Recorder
        :param args: args
        :return: bool
        """
        raise NotImplementedError("A check flow must implement its process method")

    def __init__(self):
        super().__init__()
        self.record = False


class ImplUpdateGlobal(ImplRecord):
    def type(self):
        return "update_global"

    def name(self):
        raise NotImplementedError("An update_global flow must implement its name")

    def process(self, recorder, args):
        """
        check_update and update from values of recorder
        :param recorder: Recorder
        :param args: args
        :return: None
        """
        raise NotImplementedError("An update_global flow must implement its process method")

    def __init__(self):
        super().__init__()
        self.record = None


class ImplUpdateLocal(ImplRecord):
    def type(self):
        return "update_local"

    def name(self):
        raise NotImplementedError("An update_local flow must implement its name")

    def process(self, recorder, args):
        """
        check_update and update from values of recorder
        :param recorder: Recorder
        :param args: args
        :return: None
        """
        raise NotImplementedError("An update_local flow must implement its process method")

    def __init__(self):
        super().__init__()
        self.record = None


class ImplUpdateSlot(ImplRecord):
    def type(self):
        return "update_slot"

    def name(self):
        raise NotImplementedError("An update_global flow must implement its name")

    def slots_for_init_text(self) -> list:
        raise NotImplementedError("An update_slot flow must define init values for generate-tts-processing")

    def process(self, recorder, args):
        """
        check_update and update from values of recorder
        :param recorder: Recorder
        :param args: args
        :return: None
        """
        raise NotImplementedError("An update_global flow must implement its process method")

    def __init__(self):
        super().__init__()
        self.record = None
