from config_platform import (
    PATH_CONDITIONS_DUMPED,
    PATH_CHECK_CLASS_TEMPLATE,
    PATH_UPDATE_GLOBAL_CLASS_TEMPLATE,
    PATH_UPDATE_LOCAL_CLASS_TEMPLATE,
    PATH_UPDATE_SLOT_CLASS_TEMPLATE,
    PATH_HEADER_CONDITIONS_TEMPLATE,
    PATH_CONDITION_CLASSES,
)
from src.core.utils.io import jsonio
from src.dev.context import condition_classes as cc
from src.core.utils.classes import classes_enumeration


class ConditionsInitializer(object):
    def __init__(self):
        with open(PATH_CONDITIONS_DUMPED, "r") as filein:
            self.conditions_dumped = jsonio.load_utf8(filein)

        with open(PATH_HEADER_CONDITIONS_TEMPLATE) as filein:
            self.header_conditions_python = filein.read()

        self.templates = {}
        with open(PATH_CHECK_CLASS_TEMPLATE, "r") as filein:
            self.templates["check"] = filein.read()
        with open(PATH_UPDATE_GLOBAL_CLASS_TEMPLATE, "r") as filein:
            self.templates["update_global"] = filein.read()
        with open(PATH_UPDATE_LOCAL_CLASS_TEMPLATE, "r") as filein:
            self.templates["update_local"] = filein.read()
        with open(PATH_UPDATE_SLOT_CLASS_TEMPLATE, "r") as filein:
            self.templates["update_slot"] = filein.read()

        condition_objects, condition_objects_name, self.condition_classes_source = \
            classes_enumeration.extract_python_classes(cc.__name__)

        for condition_id in self.conditions_dumped:
            if condition_id not in condition_objects_name:
                condition = self.conditions_dumped.get(condition_id)
                class_name = f"Condition{condition_id}"
                condition_desc = condition.get("desc")
                condition_type = condition.get("type")
                if condition_type in self.templates:
                    template = self.templates.get(condition_type)
                    class_source = self.fill_condition_template(template, class_name, condition_id, condition_desc)
                    self.condition_classes_source.append(class_source)
        self.make_conditions_classes_python()

    @staticmethod
    def fill_condition_template(template, class_name, condition_id, condition_desc):
        template = template.replace("CLASS_NAME", class_name)
        template = template.replace("CONDITION_ID", condition_id)
        template = template.replace("CONDITION_DESC", condition_desc)
        return template

    def make_conditions_classes_python(self):
        content = self.header_conditions_python
        sorted(self.condition_classes_source)
        for condition_class in self.condition_classes_source:
            content += "\n\n" + condition_class
        with open(PATH_CONDITION_CLASSES, "w") as fileout:
            fileout.write(content)
