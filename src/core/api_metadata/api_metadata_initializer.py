from config_platform import (
    PATH_API_METADATA_TEMPLATE,
    PATH_HEADER_API_METADATA_TEMPLATE,
    PATH_API_METADATA_DUMPED,
    PATH_API_METADATA_CLASSES
)
from src.core.utils.io import jsonio
from src.core.api_metadata import api_metadata_classes as amc
from src.core.utils.classes import classes_enumeration


class APIMetadataInitializer(object):
    def __init__(self):
        with open(PATH_API_METADATA_DUMPED, "r") as filein:
            self.api_metadata_dumped = jsonio.load_utf8(filein)

        with open(PATH_HEADER_API_METADATA_TEMPLATE) as filein:
            self.header_api_metadata_python = filein.read()

        with open(PATH_API_METADATA_TEMPLATE, "r") as filein:
            self.template = filein.read()

        api_metadata_classes_class, self.api_metadata_classes_source = classes_enumeration.get_classes(amc.__name__)
        api_metadata_objects = [clazz() for clazz in api_metadata_classes_class]
        api_metadata_objects_name = [obj.name() for obj in api_metadata_objects]

        for api_id in self.api_metadata_dumped:
            if api_id not in api_metadata_objects_name:
                api_metadata = self.api_metadata_dumped.get(api_id)
                class_name = f"API{api_id}"
                api_desc = api_metadata.get("desc")
                class_source = self.fill_condition_template(self.template, class_name, api_id, api_desc)
                self.api_metadata_classes_source.append(class_source)
        self.make_api_classes_python()

    @staticmethod
    def fill_condition_template(template, class_name, condition_id, condition_desc):
        template = template.replace("CLASS_NAME", class_name)
        template = template.replace("CONDITION_ID", condition_id)
        template = template.replace("CONDITION_DESC", condition_desc)
        return template

    def make_api_classes_python(self):
        content = self.header_api_metadata_python
        sorted(self.api_metadata_classes_source)
        for condition_class in self.api_metadata_classes_source:
            content += "\n\n" + condition_class
        with open(PATH_API_METADATA_CLASSES, "w") as fileout:
            fileout.write(content)
