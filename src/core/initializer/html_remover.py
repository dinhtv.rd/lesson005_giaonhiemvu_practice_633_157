import re


def remove_html(html_text):
    html_text = re.sub(r"<[^>]+>", "", html_text)
    html_text = re.sub("&nbsp;", " ", html_text)
    return html_text.strip()
