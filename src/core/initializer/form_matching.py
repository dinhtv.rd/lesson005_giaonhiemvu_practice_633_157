def match_style(style, determiner):
    if style is None or determiner is None:
        return False
    for key in determiner:
        if style.get(key) != determiner.get(key):
            return False
    return True


def text2style(text):
    if not text or not isinstance(text, str):
        return None
    style = {}
    params = text.split(";")
    for param in params:
        if "=" in param:
            key, value = param.split("=", 1)
            style[key] = value
        else:
            key = param
            style[key] = True
    return style
