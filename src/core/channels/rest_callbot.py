import rasa
from rasa.core.channels.channel import UserMessage, CollectingOutputChannel, InputChannel
from rasa.core.channels.rest import QueueOutputChannel

import inspect
import logging
from asyncio import Queue, CancelledError
from sanic import Blueprint, response
from sanic.request import Request
from typing import Text, Dict, Any, Optional, Callable, Awaitable

import rasa.utils.endpoints
from sanic.response import HTTPResponse
from rasa.shared.core.domain import Domain
import asyncio

from config import (
    PATH_DOMAIN,
    CONFIG_DOMAIN,
    DOMAIN_START_ACTIONS,
    DOMAIN_START_INTENTS,
    DEFAULT_USER_INFO,
    ENDPOINT_DATABASE,
    DB_KEY_HISTORY,
    DB_KEY_USER_INFORMATION,
)
from src.core.channels.rest_stepup import RestStepUp

from src.core.database.context_store import RedisStore
from src.core.database.database_connection import DatabaseConnection
from src.core.database.user_store import UserInfoStore

from src.dev.postprocessors.tts.tts_slot_processor import PostProcessTTSSlot
from src.dev.preprocessors.metadata.metadata_processor import PreprocessingExternalMetadata
from src.dev.preprocessors.message.message_processor import PreprocessingMessage
from src.core.utils.io import jsonio

logger = logging.getLogger(__name__)


class RestCallbot(InputChannel):
    """A custom http input channel.

    This implementation is the basis for a custom implementation of a chat
    frontend. You can customize this to send messages to Rasa Core and
    retrieve responses from the agent."""

    def __init__(self):
        self.history_table = ENDPOINT_DATABASE.get(DB_KEY_HISTORY)
        self.user_information_table = ENDPOINT_DATABASE.get(DB_KEY_USER_INFORMATION)

        self.start_actions = CONFIG_DOMAIN.get(DOMAIN_START_ACTIONS).copy()
        self.start_intents = CONFIG_DOMAIN.get(DOMAIN_START_INTENTS).copy()
        self.path_domain = PATH_DOMAIN
        self.domain = Domain.load(self.path_domain)

        self.connect = DatabaseConnection(host=ENDPOINT_DATABASE.get("host"),
                                          username=ENDPOINT_DATABASE.get("username"),
                                          password=ENDPOINT_DATABASE.get("password"),
                                          db=ENDPOINT_DATABASE.get("db"),
                                          port=ENDPOINT_DATABASE.get("port"))
        self.config_user_info = DEFAULT_USER_INFO.copy()
        self.PARAMS_INT = []
        self.PARAMS_FLOAT = []
        self.PARAMS_STR = []
        self.PARAMS_ARR = []
        self.PARAMS_BOOL = []
        for key in self.config_user_info:
            value = self.config_user_info.get(key)
            if isinstance(value, int):
                self.PARAMS_INT.append(key)
            elif isinstance(value, float):
                self.PARAMS_FLOAT.append(key)
            elif isinstance(value, str):
                self.PARAMS_STR.append(key)
            elif isinstance(value, list):
                self.PARAMS_ARR.append(key)
            elif isinstance(value, bool):
                self.PARAMS_BOOL.append(key)

        self.user_info_store = UserInfoStore()
        self.redis_more_store = RedisStore()
        self.external_metadata = PreprocessingExternalMetadata()
        self.preprocess_message = PreprocessingMessage()

    @classmethod
    def name(cls) -> Text:
        return "rest_custom"

    @staticmethod
    async def on_message_wrapper(
            on_new_message: Callable[[UserMessage], Awaitable[Any]],
            text: Text,
            queue: Queue,
            sender_id: Text,
            input_channel: Text,
            metadata: Optional[Dict[Text, Any]],
    ) -> None:
        collector = QueueOutputChannel(queue)

        message = UserMessage(
            text, collector, sender_id, input_channel=input_channel, metadata=metadata
        )
        await on_new_message(message)

        await queue.put("DONE")  # py_type: disable=bad-return-type

    async def _extract_sender(self, req: Request) -> Optional[Text]:
        return req.json.get("sender", None)

    async def _extract_conversation_id(self, req: Request) -> Optional[Text]:
        return req.json.get("conversation_id", None)

    async def _extract_metadata(self, req: Request, recorder=None) -> Optional[Dict[Text, Any]]:
        result = {
            "user_metadata": req.json.get("metadata", None),
            "bot_metadata": None
        }
        message = self._extract_message(req)
        await self.external_metadata.process(message, recorder)
        result["bot_metadata"] = self.external_metadata.get_metadata()
        logger.info(f"[REST] User Message: {message}")
        logger.info(f"[REST] External metadata: {result}")
        return result

    # noinspection PyMethodMayBeStatic
    def _extract_message(self, req: Request) -> Optional[Text]:
        return req.json.get("message", None)

    def _extract_input_channel(self, req: Request) -> Text:
        return req.json.get("input_channel") or self.name()

    def stream_response(
            self,
            on_new_message: Callable[[UserMessage], Awaitable[None]],
            text: Text,
            sender_id: Text,
            input_channel: Text,
            metadata: Optional[Dict[Text, Any]],
    ) -> Callable[[Any], Awaitable[None]]:
        async def stream(resp: Any) -> None:
            q = Queue()
            task = asyncio.ensure_future(
                self.on_message_wrapper(
                    on_new_message, text, q, sender_id, input_channel, metadata
                )
            )
            result = None  # declare variable up front to avoid py_type error
            while True:
                result = await q.get()
                if result == "DONE":
                    break
                else:
                    await resp.write(jsonio.dumps_utf8(result) + "\n")
            await task

        return stream  # py_type: disable=bad-return-type

    def blueprint(
            self, on_new_message: Callable[[UserMessage], Awaitable[None]]
    ) -> Blueprint:
        custom_webhook = Blueprint(
            "custom_webhook_{}".format(type(self).__name__),
            inspect.getmodule(self).__name__,
        )

        # noinspection PyUnusedLocal
        @custom_webhook.route("/", methods=["GET"])
        async def health(request: Request) -> HTTPResponse:
            return response.json({
                "status": 0,
                "msg": "Success"
            })

        # getIntents
        @custom_webhook.route("/getIntents", methods=["GET"])
        async def get_intents(request: Request) -> HTTPResponse:
            list_intents = self.domain.intents
            return response.json({
                "status": 0,
                "msg": "Success",
                "intents": list_intents,
            })

        # getEntities
        @custom_webhook.route("/getEntities", methods=["GET"])
        async def get_entities(request: Request) -> HTTPResponse:
            list_entities = self.domain.entities
            print(self.domain)
            return response.json({
                "status": 0,
                "msg": "Success",
                "entities": list_entities,
            })

        # getActions
        @custom_webhook.route("/getActions", methods=["GET"])
        async def get_actions(request: Request) -> HTTPResponse:
            list_actions = self.domain.user_actions
            return response.json({
                "status": 0,
                "msg": "Success",
                "actions": list_actions,
            })

        # inputSlots
        @custom_webhook.route("/inputSlots", methods=["GET"])
        async def input_slots(request: Request) -> HTTPResponse:
            list_slots = [key for key in self.config_user_info]

            return response.json({
                "status": 0,
                "msg": "Success",
                "inputSlots": list_slots,
            })

        @custom_webhook.route("/updateSlots", methods=["POST"])
        async def update_slots(request: Request) -> HTTPResponse:
            id_conversation = request.json.get("id_conversation")

            name_slot = [key for key in self.config_user_info]
            slots_update = request.json.get("input_slots")
            if len(slots_update) == 0:
                return response.json({
                    "status": 0,
                    "msg": "Success",
                })

            set_slots = ""
            for key in slots_update:
                if key in name_slot:
                    name = key
                    value = slots_update.get(key)
                    set_slots = set_slots + f"{name}='{value}',"
            set_slots = set_slots[:-1]
            sql = f"UPDATE {self.user_information_table} " \
                  f"SET {set_slots} " \
                  f"WHERE id_conversation='{id_conversation}'"

            connection = self.connect.get_connection()
            mycursor = connection.cursor()
            mycursor.execute(sql)
            connection.commit()
            mycursor.close()
            connection.close()

            return response.json({
                "status": 0,
                "msg": "Success",
            })

        # getTemplateAction
        @custom_webhook.route("/getTemplateAction", methods=["POST"])
        async def get_template_action(request: Request) -> HTTPResponse:
            id_conversation = request.json.get("id_conversation")
            user_info = self.user_info_store.get(id_conversation)

            record = {}
            for name_info in user_info:
                value = user_info.get(name_info)
                if value is not None:
                    record[name_info] = value

            templates = RestStepUp().templates
            answer = []
            for template in templates:
                text = template.get("text")
                for name_info in record:
                    text = text.replace(f"user_info:{name_info}", record[name_info])
                answer.append(text)

            return response.json({
                "status": 0,
                "msg": "Success",
                "action_template": answer,
            })

        # getStories
        @custom_webhook.route("/getStories", methods=["GET"])
        async def get_stories(request: Request) -> HTTPResponse:
            sql = "SELECT pre_action, action_name, intent_name FROM "
            sql += self.history_table
            connection = self.connect.get_connection()
            cursor = connection.cursor()
            connection.commit()
            cursor.execute(sql)
            query_result = cursor.fetchall()
            cursor.close()
            connection.close()
            check = {}
            total = 0

            for x in query_result:
                total += 1
                tmp = "/" + x[2]
                if check.get(x[0]) is None:
                    check[x[0]] = {}
                    check[x[0]][x[1]] = {}
                    check[x[0]][x[1]][tmp] = 1
                else:
                    if check.get(x[0]).get(x[1]) is None:
                        check[x[0]][x[1]] = {}
                        check[x[0]][x[1]][tmp] = 1
                    else:
                        if check.get(x[0]).get(x[1]).get(tmp) is None:
                            check[x[0]][x[1]][tmp] = 1
                        else:
                            check[x[0]][x[1]][tmp] += 1

            result_edges = self.result_stories

            for x in result_edges:
                pre_node = x['pre_node']
                cur_node = x['cur_node']
                intent = x['intent']
                if check.get(pre_node) is not None and check.get(pre_node).get(cur_node) is not None and check.get(
                        pre_node).get(cur_node).get(intent) is not None:
                    x['count'] = check.get(pre_node).get(cur_node).get(intent)
            list_nodes = ["START"] + self.domain.user_actions + ["END"]
            return response.json({
                "status": 0,
                "msg": "Success",
                "nodes": list_nodes,
                "edges": result_edges,
                "total_conversation": total
            })

        # initialize of calling
        @custom_webhook.route("/initCall", methods=["POST"])
        async def init_call(request: Request) -> HTTPResponse:
            id_conversation = request.json.get("id_conversation")
            name_slot = [key for key in self.config_user_info]
            keyword = ",".join(name_slot)
            tmp_key = []
            for _ in range(len(name_slot) + 1):
                tmp_key.append("%s")
            value_key = ",".join(tmp_key)
            sql = "INSERT INTO "
            if len(name_slot) > 0:
                sql += self.user_information_table + " (id_conversation," + keyword + ") VALUES (" + value_key + ")"
            else:
                sql += self.user_information_table + " (id_conversation) VALUES (" + value_key + ")"
            val = [id_conversation]
            for name in name_slot:
                if request.json.get("input_slots") is not None:
                    if name in self.PARAMS_ARR:
                        val.append(jsonio.dumps_utf8(request.json.get("input_slots").get(name)))
                    else:
                        val.append(request.json.get("input_slots").get(name))

            connection = self.connect.get_connection()
            cursor = connection.cursor()
            cursor.execute(sql, tuple(val))
            connection.commit()
            cursor.close()
            connection.close()

            return response.json({
                "status": 0,
                "msg": "Success",
            })

        # getConversation
        @custom_webhook.route("/getConversation", methods=["POST"])
        async def get_conversation(request: Request) -> HTTPResponse:
            id_conversation = request.json.get("id_conversation")
            if id_conversation is None:
                return response.json({
                    "status": -1,
                    "msg": "Fail",
                    "conversation": None,
                })
            sql = "SELECT pre_action, timestamp, intent_name, action_name, text_user, text_bot, entities FROM "
            sql += self.history_table + " WHERE sender_id = %s"
            connection = self.connect.get_connection()
            cursor = connection.cursor()
            connection.commit()
            cursor.execute(sql, [id_conversation])
            query_result = cursor.fetchall()
            result = []
            for x in query_result:
                bot_json = jsonio.loads_utf8(x[5])
                if x[3] in self.start_actions and x[2] in self.start_intents:
                    result = [{
                        "pre_action": "START",
                        "timestamp": x[1],
                        "intent_name": x[2],
                        "action_name": x[3],
                        "text_user": x[4],
                        "text_bot": bot_json['text'],
                        "text_tts_bot": bot_json['text_tts'],
                        "status_bot": bot_json['status'],
                        "entities": x[6]
                    }]
                else:
                    result.append({
                        "pre_action": x[0],
                        "timestamp": x[1],
                        "intent_name": x[2],
                        "action_name": x[3],
                        "text_user": x[4],
                        "text_bot": bot_json['text'],
                        "text_tts_bot": bot_json['text_tts'],
                        "status_bot": bot_json['status'],
                        "entities": x[6]
                    })

            sql = f"SELECT report FROM {self.user_information_table} WHERE id_conversation = %s"
            cursor.execute(sql, [id_conversation])
            rows = cursor.fetchall()
            cursor.close()
            connection.close()
            report = None
            if len(rows) > 0 and rows[0][0] is not None:
                report = jsonio.loads_utf8(rows[0][0])

            return response.json({
                "status": 0,
                "msg": "Success",
                "conversation": result,
                "report": report,
            })

        @custom_webhook.route("/webhook", methods=["POST"])
        async def receive(request: Request) -> HTTPResponse:
            sender_id = await self._extract_conversation_id(request)
            logger.debug("========Input Request====: {}".format(request.json))
            message = self._extract_message(request)
            message = self.preprocess_message.process(message)
            if self.redis_more_store.check_available_conversation_id(sender_id):
                recorder = self.redis_more_store.get_data(sender_id).tickets["recorder"]
            else:
                recorder = None
            should_use_stream = rasa.utils.endpoints.bool_arg(
                request, "stream", default=False
            )
            input_channel = self._extract_input_channel(request)
            metadata = await self._extract_metadata(request, recorder)

            if should_use_stream:
                return response.stream(
                    self.stream_response(
                        on_new_message, message, sender_id, input_channel, metadata
                    ),
                    content_type="text/event-stream",
                )
            else:
                collector = CollectingOutputChannel()
                # noinspection PyBroadException
                try:
                    await on_new_message(
                        UserMessage(
                            message,
                            collector,
                            sender_id,
                            input_channel=input_channel,
                            metadata=metadata,
                        )
                    )
                except CancelledError:
                    logger.error(
                        "Message handling timed out for "
                        "user message '{}'.".format(message)
                    )
                except Exception:
                    logger.exception(
                        "An exception occurred while handling "
                        "user message '{}'.".format(message)
                    )
                list_collector = collector.messages
                answer = []
                for packet in list_collector:
                    tmp = {}
                    tmp["conversation_id"] = packet.get("recipient_id")
                    action_packet = packet.get("text")
                    action_packet = jsonio.loads_utf8(action_packet)
                    for key in action_packet:
                        value = action_packet.get(key)
                        tmp[key] = value
                    answer.append(tmp)
                return response.json(answer[0])

        return custom_webhook
