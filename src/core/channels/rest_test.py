import rasa
from rasa.core.channels.channel import UserMessage, CollectingOutputChannel, InputChannel
from rasa.core.channels.rest import QueueOutputChannel

import inspect
import logging
from sanic import Blueprint, response
from sanic.request import Request
from typing import Text, Dict, Any, Optional, Callable, Awaitable

from sanic.response import HTTPResponse
from rasa.shared.core.domain import Domain
import json

from config import (
    DEFAULT_USER_INFO,
    PATH_DOMAIN,
    ENDPOINT_DATABASE,
    DB_KEY_FLOW_TESTCASE,
    CONFIG_TEMPLATE_ACTION
)

from src.core.database.database_connection import DatabaseConnection
from src.dev.context.report import GetReport
from rasa.shared.core.constants import (
    DEFAULT_INTENTS,
    DEFAULT_ACTION_NAMES
)
import src.core.testing.db_testcase as db_testcase
from src.core.testing import test_flow

logger = logging.getLogger(__name__)


class RestTest(InputChannel):
    """A custom http input channel.

    This implementation is the basis for a custom implementation of a chat
    frontend. You can customize this to send messages to Rasa Core and
    retrieve responses from the agent."""

    def __init__(self):
        self.flow_testcase_table = ENDPOINT_DATABASE.get(DB_KEY_FLOW_TESTCASE)
        self.path_domain = PATH_DOMAIN
        self.domain = Domain.load(self.path_domain)
        self.report_keys = GetReport().get_value()
        if self.report_keys:
            self.report_keys = [key for key in self.report_keys]
        else:
            self.report_keys = []
        self.connect = DatabaseConnection(host=ENDPOINT_DATABASE.get("host"),
                                          username=ENDPOINT_DATABASE.get("username"),
                                          password=ENDPOINT_DATABASE.get("password"),
                                          db=ENDPOINT_DATABASE.get("db"),
                                          port=ENDPOINT_DATABASE.get("port"))

        self.template_actions = CONFIG_TEMPLATE_ACTION.copy()
        self.config_user_info = DEFAULT_USER_INFO.copy()

    @classmethod
    def name(cls) -> Text:
        return "rest_test"

    async def _extract_sender(self, req: Request) -> Optional[Text]:
        return req.json.get("sender", None)

    async def _validate_testcase(self, list_params, testcase) -> Dict:
        log = []
        status = 200
        for key in list_params:
            if key not in testcase:
                log.append(f"param `{key}` must be defined!")
            elif testcase.get(key) is None:
                log.append(f"param `{key}` must be not None!")
        for key in testcase:
            value = testcase.get(key)
            if value:
                if key == "testcase_id" and not isinstance(value, int):
                    log.append(f"param `testcase_id` must be an integer!")
                if key == "input_slots" and not isinstance(value, dict):
                    log.append(f"param `input_slots` must be a dict of slot_name and value_info!")
                if key == "conversation" and not isinstance(value, list):
                    log.append(f"param `conversation` must be a list of intents and actions!")
                if key == "report" and not isinstance(value, dict):
                    log.append(f"param `report` must be dict of report params!")
                if key == "description" and not isinstance(value, str):
                    log.append(f"param `testcase_id` must be a string!")
                if key == "testing_result" and not isinstance(value, dict):
                    log.append(f"param `testing_result` must be a dict!")
        if log:
            status = 400
        return {
            "status": status,
            "msg": log
        }

    async def _extract_testcase(self, req: Request) -> Dict:
        req_json = req.json
        testcase_id = req_json.get("testcase_id")
        input_slots = req_json.get("input_slots")
        conversation = req_json.get("conversation")
        report = req_json.get("report")
        description = req_json.get("description")
        testing_result = req_json.get("testing_result")
        testcase = {
            "testcase_id": testcase_id,
            "input_slots": input_slots,
            "conversation": conversation,
            "report": report,
            "description": description,
            "testing_result": testing_result
        }
        return testcase

    def blueprint(
            self, on_new_message: Callable[[UserMessage], Awaitable[None]]
    ) -> Blueprint:
        custom_webhook = Blueprint(
            "custom_webhook_{}".format(type(self).__name__),
            inspect.getmodule(self).__name__,
        )

        # noinspection PyUnusedLocal
        @custom_webhook.route("/", methods=["GET"])
        async def health(request: Request) -> HTTPResponse:
            return response.json({
                "status": 0,
                "msg": "Success"
            })

        @custom_webhook.route("/intents", methods=["GET"])
        async def get_intents(request: Request) -> HTTPResponse:
            list_intents = self.domain.intents
            for intent_name in DEFAULT_INTENTS:
                if intent_name in list_intents:
                    list_intents.remove(intent_name)
            return response.json({
                "status": 0,
                "msg": "Success",
                "result": list_intents,
            })

        # getEntities
        @custom_webhook.route("/entities", methods=["GET"])
        async def get_entities(request: Request) -> HTTPResponse:
            list_entities = self.domain.entities
            print(self.domain)
            return response.json({
                "status": 0,
                "msg": "Success",
                "result": list_entities,
            })

        # getActions
        @custom_webhook.route("/actions", methods=["GET"])
        async def get_actions(request: Request) -> HTTPResponse:
            list_actions = self.domain.user_actions
            for action_name in DEFAULT_ACTION_NAMES:
                if action_name in list_actions:
                    list_actions.remove(action_name)

            return response.json({
                "status": 0,
                "msg": "Success",
                "result": list_actions,
            })

        # inputSlots
        @custom_webhook.route("/input_slots", methods=["GET"])
        async def input_slots(request: Request) -> HTTPResponse:
            list_slots = [key for key in self.config_user_info]
            list_slots.remove("report")
            return response.json({
                "status": 0,
                "msg": "Success",
                "result": list_slots,
            })

        # report_keys
        @custom_webhook.route("/report_keys", methods=["GET"])
        async def report_keys(request: Request) -> HTTPResponse:
            list_report_keys = self.report_keys
            return response.json({
                "status": 0,
                "msg": "Success",
                "result": list_report_keys,
            })

        @custom_webhook.route("/test_flow/insert", methods=["POST"])
        async def insert(request: Request) -> HTTPResponse:
            try:
                testcase = await self._extract_testcase(request)
                validate_params = ["conversation"]
                result_validate = await self._validate_testcase(validate_params, testcase)
                if result_validate.get("status") != 200:
                    return response.json(result_validate)
                result = await db_testcase.insert(self.connect, self.flow_testcase_table, testcase)
            except Exception as e:
                result = {
                    "status": 400,
                    "msg": f"Insert Error with exception: {str(e)}"
                }

            return response.json(result)

        @custom_webhook.route("/test_flow/select_by_testcase_id", methods=["POST"])
        async def select(request: Request) -> HTTPResponse:
            try:
                testcase = await self._extract_testcase(request)
                validate_params = ["testcase_id"]
                result_validate = await self._validate_testcase(validate_params, testcase)
                if result_validate.get("status") != 200:
                    return response.json(result_validate)
                testcase_id = testcase.get("testcase_id")
                result = await db_testcase.select(self.connect, self.flow_testcase_table, testcase_id)
            except Exception as e:
                result = {
                    "status": 400,
                    "msg": f"Select Error with exception: {str(e)}"
                }
            return response.json(result)

        @custom_webhook.route("/test_flow/select_all", methods=["POST"])
        async def select_all(request: Request) -> HTTPResponse:
            try:
                result = await db_testcase.select(self.connect, self.flow_testcase_table)
            except Exception as e:
                result = {
                    "status": 400,
                    "msg": f"Select Error with exception: {str(e)}"
                }
            return response.json(result)

        @custom_webhook.route("/test_flow/update_testcase", methods=["POST"])
        async def update(request: Request) -> HTTPResponse:
            try:
                testcase = await self._extract_testcase(request)
                validate_params = ["testcase_id",
                                   "input_slots",
                                   "conversation",
                                   "report",
                                   "description",
                                   "testing_result"]
                result_validate = await self._validate_testcase(validate_params, testcase)
                if result_validate.get("status") != 200:
                    return response.json(result_validate)
                result = await db_testcase.update(self.connect, self.flow_testcase_table, testcase)
            except Exception as e:
                result = {
                    "status": 400,
                    "msg": f"Update Error with exception: {str(e)}"
                }
            return response.json(result)

        @custom_webhook.route("/test_flow/delete_by_testcase_id", methods=["POST"])
        async def delete(request: Request) -> HTTPResponse:
            try:
                testcase = await self._extract_testcase(request)
                validate_params = ["testcase_id"]
                result_validate = await self._validate_testcase(validate_params, testcase)
                if result_validate.get("status") != 200:
                    return response.json(result_validate)
                testcase_id = testcase.get("testcase_id")
                result = await db_testcase.delete(self.connect, self.flow_testcase_table, testcase_id)
            except Exception as e:
                result = {
                    "status": 400,
                    "msg": f"Delete Error with exception: {str(e)}"
                }
            return response.json(result)

        @custom_webhook.route("/test_flow/run", methods=["POST"])
        async def run_test_flow(request: Request) -> HTTPResponse:
            try:
                result_select = await db_testcase.select(self.connect, self.flow_testcase_table)
                list_testcases = result_select.get("testcases")
                result = await test_flow.run_testcases(list_testcases)
                result = {
                    "status": 0,
                    "msg": "Success",
                    "result": result
                }
            except Exception as e:
                result = {
                    "status": 400,
                    "msg": f"Run flow test Error with exception: {str(e)}"
                }
            return response.json(result)

        @custom_webhook.route("/test_flow/run_and_update", methods=["POST"])
        async def run_and_update(request: Request) -> HTTPResponse:
            try:
                result_select = await db_testcase.select(self.connect, self.flow_testcase_table)
                list_testcases = result_select.get("testcases")
                result = await test_flow.run_testcases(list_testcases)
                for idx, testcase in enumerate(result):
                    try:
                        result_update = await db_testcase.update(self.connect, self.flow_testcase_table, testcase)
                    except Exception as e:
                        result_update = {
                            "status": 400,
                            "msg": f"Update Error with exception: {str(e)}"
                        }
                    result[idx].update({
                        "status_update": result_update
                    })
                result = {
                    "status": 0,
                    "msg": "Success",
                    "result": result
                }
            except Exception as e:
                result = {
                    "status": 400,
                    "msg": f"Run flow test Error with exception: {str(e)}"
                }
            return response.json(result)

        @custom_webhook.route("/test_flow/run_by_testcase_id", methods=["POST"])
        async def run_test_flow_by_testcase_id(request: Request) -> HTTPResponse:
            try:
                testcase = await self._extract_testcase(request)
                validate_params = ["testcase_id"]
                result_validate = await self._validate_testcase(validate_params, testcase)
                if result_validate.get("status") != 200:
                    return response.json(result_validate)
                testcase_id = testcase.get("testcase_id")
                result_select = await db_testcase.select(self.connect, self.flow_testcase_table, testcase_id)
                list_testcases = result_select.get("testcases")
                result = await test_flow.run_testcases(list_testcases)
                result = {
                    "status": 0,
                    "msg": "Success",
                    "result": result
                }
            except Exception as e:
                result = {
                    "status": 400,
                    "msg": f"Run flow test Error with exception: {str(e)}"
                }
            return response.json(result)

        @custom_webhook.route("/test_flow/run_and_update_by_testcase_id", methods=["POST"])
        async def run_and_update_test_flow_by_testcase_id(request: Request) -> HTTPResponse:
            try:
                testcase = await self._extract_testcase(request)
                validate_params = ["testcase_id"]
                result_validate = await self._validate_testcase(validate_params, testcase)
                if result_validate.get("status") != 200:
                    return response.json(result_validate)
                testcase_id = testcase.get("testcase_id")
                result_select = await db_testcase.select(self.connect, self.flow_testcase_table, testcase_id)
                list_testcases = result_select.get("testcases")
                result = await test_flow.run_testcases(list_testcases)
                for idx, testcase in enumerate(result):
                    try:
                        result_update = await db_testcase.update(self.connect, self.flow_testcase_table, testcase)
                    except Exception as e:
                        result_update = {
                            "status": 400,
                            "msg": f"Update Error with exception: {str(e)}"
                        }
                    result[idx].update({
                        "status_update": result_update
                    })
                result = {
                    "status": 0,
                    "msg": "Success",
                    "result": result
                }
            except Exception as e:
                result = {
                    "status": 400,
                    "msg": f"Run flow test Error with exception: {str(e)}"
                }
            return response.json(result)

        return custom_webhook
