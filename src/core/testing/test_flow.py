import aiohttp
import logging
from datetime import datetime
import random
import os

from config import ENDPOINT_SERVER

logger = logging.getLogger(__name__)

MAXINT = 1000000000 + 7


def gen_conversation_id():
    now = datetime.now()
    timestamp = datetime.timestamp(now)
    int_rand = random.randint(0, MAXINT)
    conversation_id = f"test-{timestamp}-{int_rand}"
    return conversation_id


async def compare_conversation(conversation, predicted_conversation):
    if conversation is None:
        return "null", "No conversation to compare"
    for idx, line_test in enumerate(conversation):
        line_pred = predicted_conversation[idx]
        if line_pred != line_test:
            return "failed", f"Test failed in line {idx}: {line_test} != {line_pred}"
    return "passed", "passed"


async def compare_report(report, predicted_report):
    if report is None:
        return "null", "No report to compare"
    status = "passed"
    log = []
    for key in report:
        if key in predicted_report:
            value = report.get(key)
            predicted_value = predicted_report.get(key)
            if value != predicted_value:
                log.append(f"`{key}` is not equal: {value} (test) != {predicted_value} (pred)")
        else:
            log.append(f"`{key}` not in predicted_report")
    if not log:
        log = "passed"
    else:
        status = "failed"
    return status, log


async def _extract_testcase(testcase):
    testcase_id = testcase.get("testcase_id")
    input_slots = testcase.get("input_slots")
    conversation = testcase.get("conversation")
    report = testcase.get("report")
    description = testcase.get("description")
    return testcase_id, input_slots, conversation, report, description


async def simulate_conversation(input_slots, conversation):
    url_init = os.path.join(ENDPOINT_SERVER, "rest_custom/initCall")
    url_webhook = os.path.join(ENDPOINT_SERVER, "rest_custom/webhook")

    conversation_id = gen_conversation_id()
    predicted_conversation = []
    predicted_report = None

    if input_slots:
        headers = {'Content-Type': 'application/json'}
        data = {
            "id_conversation": conversation_id,
            "input_slots": input_slots,
        }
        async with aiohttp.ClientSession() as session:
            async with session.post(url_init, json=data, headers=headers) as response:
                r = await response.json()

    for idx, message in enumerate(conversation):
        is_intent = idx % 2 == 0
        if is_intent:
            message_intent = f"_{message}"

            headers = {'Content-Type': 'application/json'}
            data = {
                "conversation_id": conversation_id,
                "message": message_intent,
                "metadata": {
                    "audio_url": None,
                }
            }
            async with aiohttp.ClientSession() as session:
                async with session.post(url_webhook, json=data, headers=headers) as response:
                    r = await response.json()
            predicted_report = r.get('variables').get("get_report")
            predicted_action = r.get('action')
            predicted_conversation.append(message)
            predicted_conversation.append(predicted_action)

    return predicted_conversation, predicted_report


async def run_a_testcase(testcase):
    testcase_id, input_slots, conversation, report, description = await _extract_testcase(testcase)

    predicted_conversation, predicted_report = await simulate_conversation(input_slots, conversation)
    status_test_conversation, log_test_conversation = await compare_conversation(conversation, predicted_conversation)
    status_test_report, log_test_report = await compare_report(report, predicted_report)

    result = testcase.copy()
    testing_result = {
        "testing_result": {
            # "conversation": predicted_conversation,
            # "report": predicted_report,
            "status_test_conversation": status_test_conversation,
            "log_test_conversation": log_test_conversation,
            "status_test_report": status_test_report,
            "log_test_report": log_test_report
        }
    }
    result.update(testing_result)
    return result


async def run_testcases(testcases):
    results = []
    for testcase in testcases:
        testcase_result = await run_a_testcase(testcase)
        results.append(testcase_result)
    return results
