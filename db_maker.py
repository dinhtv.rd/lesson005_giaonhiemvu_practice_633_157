import MySQLdb
from config import ENDPOINT_DATABASE
from config import DEFAULT_USER_INFO

from src.core.database.database_connection import DatabaseConnection

db_name = ENDPOINT_DATABASE.get("db")


def create_table_schema():
    connection = MySQLdb.connect(
        host=ENDPOINT_DATABASE.get("host"),
        port=ENDPOINT_DATABASE.get("port"),
        user=ENDPOINT_DATABASE.get("username"),
        passwd=ENDPOINT_DATABASE.get("password")
    )
    cursor = connection.cursor()
    cursor.execute(f"CREATE DATABASE IF NOT EXISTS `{db_name}` DEFAULT CHARACTER SET utf8 COLLATE utf8_vietnamese_ci;")
    cursor.close()
    connection.close()


def create_history():
    connection = MySQLdb.connect(
        host=ENDPOINT_DATABASE.get("host"),
        port=ENDPOINT_DATABASE.get("port"),
        user=ENDPOINT_DATABASE.get("username"),
        passwd=ENDPOINT_DATABASE.get("password"),
        db=ENDPOINT_DATABASE.get("db")
    )
    cursor = connection.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS `history` (  "
                   "`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,  "
                   "`sender_id` text COLLATE utf8_vietnamese_ci,  "
                   "`pre_action` text COLLATE utf8_vietnamese_ci,  "
                   "`timestamp` int(11) DEFAULT NULL,  "
                   "`intent_name` text COLLATE utf8_vietnamese_ci,  "
                   "`action_name` text COLLATE utf8_vietnamese_ci,  "
                   "`text_user` text COLLATE utf8_vietnamese_ci,  "
                   "`text_bot` text COLLATE utf8_vietnamese_ci,  "
                   "`entities` text COLLATE utf8_vietnamese_ci,  "
                   "PRIMARY KEY (`id`)) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;")
    cursor.close()
    connection.close()


def create_user_information():
    connection = MySQLdb.connect(
        host=ENDPOINT_DATABASE.get("host"),
        port=ENDPOINT_DATABASE.get("port"),
        user=ENDPOINT_DATABASE.get("username"),
        passwd=ENDPOINT_DATABASE.get("password"),
        db=ENDPOINT_DATABASE.get("db")
    )
    cursor = connection.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS `user_information` ("
                   "`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,  "
                   "`id_conversation` text COLLATE utf8_vietnamese_ci,  "
                   "`report` longtext COLLATE utf8_vietnamese_ci,  "
                   "PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;")
    cursor.close()
    connection.close()


def alter_columns():
    connection = MySQLdb.connect(
        host=ENDPOINT_DATABASE.get("host"),
        port=ENDPOINT_DATABASE.get("port"),
        user=ENDPOINT_DATABASE.get("username"),
        passwd=ENDPOINT_DATABASE.get("password"),
        db=ENDPOINT_DATABASE.get("db")
    )
    cursor = connection.cursor()
    for key in DEFAULT_USER_INFO:
        if key not in ["report", "id_conversation", "id"]:
            try:
                query = f"ALTER TABLE `user_information` ADD `{key}` TEXT NULL DEFAULT NULL AFTER `report`;"
                cursor.execute(query)
            except Exception as e:
                print(f"Can't execute query {query} with exception {e}")
    cursor.close()
    connection.close()


create_table_schema()
create_history()
create_user_information()
alter_columns()
print(f"Create DB successful!")
