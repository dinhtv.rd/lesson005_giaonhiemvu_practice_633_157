#!/bin/bash

. run_config.sh
export CUDA_VISIBLE_DEVICES=-1
export TF_INTRA_OP_PARALLELISM_THREADS=8
export TF_INTER_OP_PARALLELISM_THREADS=8

if [ -f $PATH_PID/rasa_train.pid ] && [ $(cat $PATH_PID/rasa_train.pid | wc -w) -gt 0 ]; then
  pid_train=$(cat $PATH_PID/rasa_train.pid)
  echo "KILL PID TRAIN: $pid_train"
  ps -o pid= --ppid $pid_train
  kill -9 $(ps -o pid= --ppid $pid_train)
  kill -9 $pid_train
  rm $PATH_PID/rasa_train.pid
fi

nohup $bin/python main.py train --debug > $PATH_LOG/rasa_train.log &
pid_train=$(echo $!)
echo $pid_train > $PATH_PID/rasa_train.pid
echo "Started rasa train - Log in $PATH_LOG/rasa_train.log - PID $pid_train"

