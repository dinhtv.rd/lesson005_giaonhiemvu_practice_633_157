# Document for making Classification

## Modules

* NLU deep learning model
* Regex intent module

## NLU Model

* Config NLU modules in pipeline in file `config.yml`
* Sources: `src/nlu/classifier`

## Preparing Data

* Path: `data/nlu/*/*.md`
* Form:

```markdown
## intent:name_intent

- sample 1
- sample 2
- sample 3
```

* Example:

```markdown
## intent:affirm_confirm

- đồng ý
- ok em ơi
- được nhé em
```