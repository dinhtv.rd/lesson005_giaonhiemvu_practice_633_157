# Document for making Named Entity Recognition

## Modules

* Regex entity
* Special case: Recognize with code
* NLU: almost not used because entities are often easily identifiable with regex
* Other Model: during construction

## Make

* Regex: define list patterns for each entity

    * **Example**: file `data/nlu/entities/phone_number.md`

* Each message contains only one entity for each type, so if it exists many entities, you will define the select
  function to make the best choice:
    * perform coding in `src/nlu/ner/regex_selector.py`

```markdown
## regex:phone_number

- \b(0|84)([0-9]{9}|[0-9]{10})\b
```

* Special case: code in `src/nlu/ner/regex_special_case.py`
* Other case: get entity with context in recorder `src/utils/records.py`, `src/utils/recorder.py`